## Exercise 1

Create an single screen app that has one text input that accepts a valid URL and then immediately loads the web-page in a “WebView” component.

-- As my first react-native project, and first experience developing in React-Native, it's amazing to see how simple a functional (and purposeful) application can be to developed, especially by how much responsive magic the WebView component does by itself.

* [Available Scripts](#available-scripts)
  * [npm start](#npm-start)
  * [npm run ios](#npm-run-ios)
  * [npm run android](#npm-run-android)

#### `npm run ios`

Like `npm start`, but also attempts to open your app in the iOS Simulator if you're on a Mac and have it installed.

#### `npm run android`

Like `npm start`, but also attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup).
