import React from "react";
import {
  StyleSheet,
  Text,
  View,
  WebView,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  TextInput
} from "react-native";

export default class WebsiteViewer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      url: "",
      isValid: false,
      webViewError: false
    };
  }

  validateURI(event) {
    const input = event.nativeEvent.text;
    const baseRegex = "^([a-z0-9]+[.])+[a-z.]+(/?[a-zA-Z0-9#]+/?)*$";
    let validationRegex;

    //Different Regex needs to be applied against strings prefixed in 'www.'
    if (/^[w]{1,3}\./gi.test(input)) {
      validationRegex = `${baseRegex.slice(0, 1)}www\.+${baseRegex.slice(1)}`;
    }

    const regex = new RegExp(
      validationRegex ? validationRegex : baseRegex,
      "gi"
    );

    this.setState({
      isValid: regex.test(input)
    });
  }

  submitURL(event) {
    this.state.isValid &&
      this.setState({ url: `https://${event.nativeEvent.text}` });
  }

  webViewError() {
    this.setState({ url: "", isValid: false, webViewError: true });
  }

  render() {
    if (this.state.url) {
      return (
        <WebView
          source={{ uri: this.state.url }}
          startInLoadingState={true}
          onError={this.webViewError.bind(this)}
          style={{ flex: 1, marginTop: 20 }}
        />
      );
    } else {
      const statusText = !this.state.webViewError
        ? "Please enter a valid URL:"
        : "Something went wrong. \nPlease try again.";

      return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            <KeyboardAvoidingView
              behavior="padding"
              keyboardVerticalOffset={-500}
              style={styles.homeContainer}
            >
              <View>
                {/* Wrapped in View component to smoothen KeyboardAvoidingView animation */}
                <Text style={{ fontSize: 20 }}>{statusText}</Text>
              </View>

              <TextInput
                placeholder="www."
                onChange={this.validateURI.bind(this)}
                onSubmitEditing={this.submitURL.bind(this)}
                autoCapitalize="none"
                returnKeyType="go"
                style={[
                  styles.inputBase,
                  this.state.isValid ? styles.inputValid : styles.inputInvalid
                ]}
              />
            </KeyboardAvoidingView>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  }
}

const styles = StyleSheet.create({
  homeContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center"
  },
  inputBase: {
    fontSize: 20,
    marginTop: 30,
    padding: 10,
    width: "80%"
  },
  inputInvalid: {
    borderColor: "lightgrey",
    borderWidth: 2
  },
  inputValid: {
    borderColor: "lightgreen",
    borderWidth: 2
  }
});
